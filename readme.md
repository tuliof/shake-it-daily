# Shake it daily!

This is a study app born to help me re-learn Android development.

It's meant to be used in my daily SCRUM meetings, to control the time each one has to speak thus keeping the meeting short and meaningful.

## How does it work? ##
1. Grab the phone
2. Shake it to start your time
3. Tell us:

* What you did yesterday
* What you are doing today
* Are there any impediments?

**If your time is up, the phone will vibrate, pass it along!**

### Links for me to check
http://appicontemplate.com/android

