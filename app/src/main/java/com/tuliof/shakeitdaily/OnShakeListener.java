package com.tuliof.shakeitdaily;

/**
 * Created by saotfern on 17/09/2015.
 *
 * Definition for OnShakeListener definition. I would normally put this
 * into it's own .java file, but I included it here for quick reference
 * and to make it easier to include this file in our project.
 */
public interface OnShakeListener {
    void onShake();
}
