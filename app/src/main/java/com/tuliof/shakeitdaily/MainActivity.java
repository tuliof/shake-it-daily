package com.tuliof.shakeitdaily;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;


public class MainActivity extends AppCompatActivity {

    private static final Logger logger = Logger.getLogger("Main");
    private static final int COOL_DOWN_TIME = 1000; //ms
    private static long lastShakeEvent;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private OnShakeListener mSensorListenerX;
    private SensorEventListener mSensorListener;
    private AtomicInteger counter = new AtomicInteger(1);
    private ArrayList<Long> times = new ArrayList<>();
    private Chronometer cronometro;
    private Button btnStartStop;

    @Override
    protected void onResume() {
        super.onResume();

        setupAccelerometer();

        if (null == accelerometer) {
            Toast.makeText(MainActivity.this, "Dispositivo não possuí acelerômetro.",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "onResume acelerômetro iniciado.",
                    Toast.LENGTH_SHORT).show();

            sensorManager.registerListener(mSensorListener, accelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cronometro = (Chronometer) findViewById(R.id.cronometro);
        btnStartStop = (Button) findViewById(R.id.btnStartStop);

        lastShakeEvent = System.currentTimeMillis();
    }

    protected Sensor setupAccelerometer() {
        Sensor accelerometer = null;
        logger.info("Seting up accelerometer.");
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        OnShakeListener shakeListener = new OnShakeListener() {

            public void onShake() {

                Toast.makeText(MainActivity.this, "Shake!", Toast.LENGTH_SHORT).show();
                if (System.currentTimeMillis() > (lastShakeEvent + COOL_DOWN_TIME)) {
                    Toast.makeText(MainActivity.this, "Shake! " + counter.getAndAdd(1),
                            Toast.LENGTH_SHORT).show();

                    /*Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);*/

                    times.add(System.currentTimeMillis());

                    btnStartStop_Click(findViewById(R.id.btnStartStop));

                    lastShakeEvent = System.currentTimeMillis();
                }
            }
        };

        mSensorListener = new ShakeDetector(shakeListener);

        List<Sensor> sensorList = new ArrayList<>();
        sensorList.addAll(sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER));

        if (sensorList.isEmpty()) {
            logger.info("No accelerometer found!");
            accelerometer = null;
        }
        else {
            logger.info("Accelerometer found!");
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }

        return accelerometer;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        if (null != accelerometer)
            sensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    public void btnStartStop_Click(View view) {
        if (btnStartStop.getText().equals("Start")) {
            btnStartStop.setText("Stop");
            restartChronometer();
        } else {
            btnStartStop.setText("Start");
            cronometro.stop();
        }
    }

    protected void restartChronometer() {
        Log.i("MainActivity", "restartChronometer!");
        cronometro.setBase(SystemClock.elapsedRealtime());
        cronometro.start();
    }

    protected void setNextDateToNotify() {
        Calendar c = Calendar.getInstance();
        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.preference_file_key),
                this.MODE_PRIVATE);

        // Get time chosen by user
        int hour = 11;
        int minutes = 15;
        String time = prefs.getString(getString(R.string.saved_daily_hour), "");
        if (!time.isEmpty()) {
            String[] times = time.split(":");
            hour = Integer.valueOf(times[0]);
            minutes = Integer.valueOf(times[1]);
        }

        // CTRL + P para ver assinatura do método
        // Get the current date & time scheduled, if none, use current date & time
        long currentScheduleInMillis = prefs.getLong(getString(R.string.saved_next_daily_in_millis), 0);
        Calendar currentSchedule = Calendar.getInstance();
        if (currentScheduleInMillis != 0) {
            currentSchedule.setTimeInMillis(currentScheduleInMillis);
        }
        else {
            // Never had a schedule, use current date and time as reference.
        }

        // Calculate the next date
        //Calendar nextSchedule = c.set(Calendar.YEAR, currentSchedule.getTime().);


        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //alarm.setInexactRepeating(AlarmManager.INTERVAL_DAY,);
        //alarm.set(AlarmManager.INTERVAL_DAY, );
        Date d = c.getTime();
       // c.set(Calendar.get(Calendar.YEAR) - 1900, Calendar.MONTH);
    }
}
